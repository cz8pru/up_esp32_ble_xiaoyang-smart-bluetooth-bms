# uP_ESP32_BLE_xiaoyang-smart-bluetooth-bms

![esp32 BMS](./fig/esp32_bms.jpg)



# Links

* https://wnsnty.xyz/entry/jbd-xiaoyang-smart-bluetooth-bms-information
* https://github.com/kolins-cz/Smart-BMS-Bluetooth-ESP32
* https://github.com/micropython/micropython/tree/master/examples/bluetooth

# Result

```
>>> %Run -c $EDITOR_CONTENT
_UART_SERVICE_UUID UUID(0xff00)
_IRQ_SCAN_DONE
Found peripheral: 0 b'\xa4\xc17 +\xbb' JBD-SP16S020-L8S
service (0, 1, 7, UUID(0x1800))
service (0, 8, 11, UUID(0x1801))
service (0, 12, 14, UUID(0x180a))
service (0, 15, 22, UUID(0xff00))
service (0, 23, 26, UUID(0xfa00))
GATTC_CHAR,RX:  17 18 UUID(0xff01)
GATTC_CHAR,TX:  21 6 UUID(0xff02)
Connected
TX bytearray(b'\xdd\xa5\x03\x00\xff\xfdw\r\n')
TX complete
TX bytearray(b'\xdd\xa5\x03\x00\xff\xfdw\r\n')
TX complete
RX b'dd:03:00:26:0a:68:00:00:2e:ab:2e:e0:00:08:2c:d7:00:00:00:00'
RX b'00:00:23:64:01:08:03:0b:8c:0b:88:0b:87:00:00:00:2e:e0:2e:ab'
RX b'00:00:f8:40:77'
disconnect:self._conn_handle: 0
The connection handle is disconnected.
Disconnected
```

## BMS DATA
https://github.com/bres55/Smart-BMS-arduino-Reader/blob/master/bms.xlsx


RX b'dd:03:00:26:0a:68:00:00:2e:ab:2e:e0:00:08:2c:d7:00:00:00:00'
RX b'00:00:23:64:01:08:03:0b:7e:0b:77:0b:73:00:00:00:2e:e0:2e:ab'
RX b'00:00:f8:73:77'

https://github.com/kolins-cz/Smart-BMS-Bluetooth-ESP32/blob/master/mydatatypes.h:

### bmsPacketHeaderStruct
| 0    | 1  |   2  |         3  |
|------|----|------|------------|
| 0xdd |0x03| 0x00 | 0x26 = 38d |
|header|type|status|data length |

checksumLen = (status) + (data length) + (data) = (data length) + 2 = 38 + 2 = 40d

packet[offset + checksumLen + 1] = packet[2 + 40 + 1] = packet[43]


### type
https://github.com/kolins-cz/Smart-BMS-Bluetooth-ESP32/blob/master/Smart-BMS-Bluetooth-ESP32.ino

cBasicInfo3 = 3; //type of packet 3= basic info
cCellInfo4 = 4;  //type of packet 4= individual cell info


### BasicInfo3
see https://github.com/kolins-cz/Smart-BMS-Bluetooth-ESP32/blob/master/BMS_process_data.ino
processBasicInfo()

| 4     | 5     | 6     | 7     | 8    | 9    |
|-------|-------|-------|-------|------|------|
| 0a    | 68    | 00    | 00    | 2e   | ab   |
| Volt0 | Volt1 | Curr2 | Curr3 | QAh4 | QAh5 |

#### Voltage
Volt0 + Volt1 = 0x0a68 = 2664 = 26.640 V

#### Current
Curr2 + Curr3 = 0x0000 =     0 =>  0.0 A

#### CapacityRemainAh 
QAh4 + QAh5   = 0x2eab = 11947 => 119.47 Ah

#### Cycle times

| 10   | 11   | 12   | 13    | 14 | 15 |
|------|------|------|-------|----|----|
| 2e   | e0   | 00   | 08    | 2c | d7 | 
| Cap6 | Cap7 | Cyc8 |  Cyc9 | 10 | 11 | 

Nominal capacait = 0x2ee0 = 12000 = 120.00 Ah

Cycle times = 0x0800 = 8 times

#### BalanceCode
BalanceCodeLow  = Bal12 + Bal13 = 00 + 00 = 0
BalanceCodeHigh = Bal14 + Bal15 = 00 + 00 = 0


#### CapacityRemainPercent 
Qprc19 = 0x64 = 100 = 100%

#### Mosfet
MosFet20 = 0x01
 
| 16    | 17    | 18    | 19    | 20     | 21     | 22 | 23     | 24       |
|-------|-------|-------|-------|--------|--------|----|--------|----------|
| 00    | 00    | 00    | 00    | 00     | 00     | 23 | 64     | 01       |
| Bal12 | Bal13 | Bal14 | Bal15 | prot16 | prot17 | 18 | Qprc19 | MosFet20 |



#### Temperature

Temp23 + Temp24 = 0x0b7e = 2942 => 2942-2731 = 211 => 21.1degC
Temp25 + Temp26 = 0x0b77 = 2935 => 2935-2731 = 204 => 20.4degC

| 25 | 26 | 27   | 28   | 29   | 30   | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 |
|----|----|------|------|------|------|----|----|----|----|----|----|----|----|
|08  | 03 | 0b   | 7e   | 0b   | 77   | 0b | 73 | 00 | 00 | 00 | 2e | e0 | 2e |
| 21 | 22 |Temp23|Temp24|Temp25|Temp26|    |    |    |    |    |    |    |    |


| 39 | 40 | 41 | 42 | 43            | 44 |
|----|----|----|----|---------------|----|
|ab  | 00 | 00 | f8 | 73            | 77 |
|    |    |    |    | rxCheckSum??? |    |



# ISSSUE

## Get Stuck

_UART_SERVICE_UUID UUID(0xff00)
_IRQ_SCAN_DONE
Found peripheral: 0 b'\xf4\x92\xbf\xde\x17\x82' ?
lld_pdu_get_tx_flush_nb HCI packet count mismatch (1, 2)
Failed to find uart service.
