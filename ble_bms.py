from ble_simple_central import BLESimpleCentral
import bluetooth
import time
from binascii import hexlify
import ustruct

#from micropython import const

_BMS_HDR = 0 #const(0)
_BMS_TYPE = 1
_BMS_STAT = 2
_BMS_LEN = 3 #const(3)

_BMS_VOLT_H =   4 # data[0]
_BMS_VOLT_L =   5 # data[1]
_BMS_AMP_H =    6 # data[2]
_BMS_AMP_L =    7 # data[3]
_BMS_CAP_AH_H = 8 # data[4]
_BMS_CAP_AH_L = 9 # data[5]
# reserved
_BMS_CAP_PRC = 23 # data[19]
_BMS_MOSFET =  24 # data[20]
# reserved
_BMS_TEMP1_H = 27 # data [23]
_BMS_TEMP1_L = 28 # data [24]
_BMS_TEMP2_H = 29 # data [25]
_BMS_TEMP2_L = 30 # data [26]

class Cbms:
    def __init__(self, reload_time):
        self.volt = None
        self.amps = None
        self.capAh = None
        self.capPrc = None
        self.temp1 = None
        self.temp2 = None
        self.mosfet = None
        self.ble_rx = bytearray()
        self.ble = bluetooth.BLE()
        self.central = BLESimpleCentral(self.ble)
        self.not_found = True
        self.power = None
        self.energy = 0
        self.energy_last = 0
        self.energy_diff = None
        self.temp = None
        self.ok_counter = 0
        self.get_counter = 0
        self.comm_ok = False
        self.reload_time = reload_time # [sec]
        self.scheduled_time = 0 # [ms]
        self.comm_ok_counter = 0
        self.first_run = True
        time.sleep_ms(20)

    def get_comm_ok(self):
        return self.comm_ok

    def comm_ok_eval(self,i_bool):
        # Once the communication is established comm_ok became True
        # 3 times in row there is error, comm_ok is False
        if (i_bool):
            # Comm OK
            self.comm_ok_counter = 3
        else:
            # Comm Error
            if (self.comm_ok_counter > 0):
                self.comm_ok_counter = self.comm_ok_counter - 1
            else:
                self.comm_ok_counter = 0
        if (self.comm_ok_counter == 0):
            self.comm_ok = False
        else:
            self.comm_ok = True

    #
    # https://github.com/kolins-cz/Smart-BMS-Bluetooth-ESP32/blob/master/BMS_process_data.ino
    #
    def is_packet_valid(self, packet):
        if (45!=len(packet)):
            return False
        

        protocol_checksum_length = packet[_BMS_LEN] + 2
        
        if (packet[_BMS_HDR] != 0xDD):
            return False
        
        offset   = 2
        checksum = 0
        
        for i in range(packet[_BMS_LEN]):
            checksum = checksum + packet[offset+i]
        
        checksum = ((checksum ^ 0xFF) + 1) & 0xFF;
        

        rxChecksum = packet[offset + protocol_checksum_length + 1];

        if (checksum == rxChecksum):
            return True;

        print(checksum, " : ", rxChecksum)
        return False;

    # Process BMS Packet    
    def bmsProcessPacket(self, packet):
        is_valid = self.is_packet_valid(packet)
        
        if (is_valid == False):
            return False
        
        protocol_checksum_length = packet[_BMS_LEN] + 2
        
        if (packet[_BMS_TYPE] != 3):
            print("protocol_type: ", packet[_BMS_TYPE]);
            return False
        
        if (protocol_checksum_length != 40): # 0x1B=27d
            print("protocol_checksum_length: ", protocol_checksum_length);
            return False
        
        self.volt = ustruct.unpack_from(">h", packet, _BMS_VOLT_H )[0] * 10 # [mV]
        self.amps = ustruct.unpack_from(">h", packet, _BMS_AMP_H )[0] * 10 # [mA]
        
        self.power = (self.volt/1000)*(self.amps/1000)
        
        self.capAh = ustruct.unpack_from(">h", packet, _BMS_CAP_AH_H )[0] # [10mAh]
        self.capPrc = packet[_BMS_CAP_PRC]
        
        
        self.energy = (self.capAh/100) * (self.volt/1000)
        
        if ( self.first_run == True ):
            # for first time take last as it is to have diff = 0
            self.first_run = False
            self.set_energy()
        
        self.energy_diff = self.energy - self.energy_last
        

        self.temp1 = ustruct.unpack_from(">h", packet, _BMS_TEMP1_H )[0]-2731
        self.temp2 = ustruct.unpack_from(">h", packet, _BMS_TEMP2_H )[0]-2731
        self.mosfet = packet[_BMS_MOSFET]
        
        self.temp = (self.temp1+self.temp2)/2
        
        return True
    
    def set_energy(self):
        self.energy_last = self.energy
        
    def get(self):

        self.get_counter = self.get_counter + 1        
        for i_time in range(20):
            if (self.bmsProcessPacket(self.ble_rx)):
                self.ble_rx = bytearray()
                self.ok_counter = self.ok_counter + 1
                self.comm_ok_eval(True)
                return self.get_comm_ok()
        
            time.sleep_ms(100)
            
        self.ble_rx = bytearray()
        self.comm_ok_eval(False)
        return self.get_comm_ok()
    
    # Periodically execute this method in infinite loop
    def handler(self):
        actual_time_ms = time.ticks_ms()
                
        if (time.ticks_diff(self.scheduled_time, actual_time_ms) > 0):
            return False
        
        if not (self.scan_and_connect()):
            # plan next try in 10 secons
            self.scheduled_time = time.ticks_add(actual_time_ms, 10 * 1000)
            self.comm_ok_eval(False)
            return self.get_comm_ok()
            
        self.data_transfer()
        
        if not (self.get()):
            # plan next try in 10 secons
            self.scheduled_time = time.ticks_add(actual_time_ms, 10 * 1000)
            self.comm_ok_eval(False)
            return self.get_comm_ok()
        
        
        # https://github.com/espressif/esp-idf/issues/5105
        # Calculate deadline for operation and test for it
        deadline = time.ticks_add(actual_time_ms, 200)
        while (time.ticks_diff(deadline, time.ticks_ms()) > 0):
            time.sleep_ms(10)
            
        self.disconnect()
        
        # when data were received, plan next execution
        self.scheduled_time = time.ticks_add(actual_time_ms, self.reload_time * 1000)
        return True
    
    def scan_and_connect(self):
        
        def on_scan(addr_type, addr, name):
            if addr_type is not None:
                #print("Found peripheral:", addr_type, addr, name)
                ret = self.central.connect()
                #print("on_scan", ret)
                
            else:
                self.not_found = True
                print("No peripheral found.")

        self.not_found = False
        self.central.scan(callback=on_scan)

        ret = False
        #print("Wait for connection...")
        for i_time in range(20):
            #print(".", end="")
            if self.central.is_connected():
                #print("Connected")
                ret = True 
                break
            
            if self.not_found:
                print("Not found")
                break
            
            time.sleep_ms(100)

        
        return True

    def data_transfer(self):
        #print("\n\ndata_transfer\n\n")
        def on_rx(v):
            self.ble_rx = self.ble_rx + v
            #print("RX", hexlify(v, ":"))

        self.central.on_notify(on_rx)

        with_response = True

        for x in range(2):
            if self.central.is_connected():
                try:
                    v = bytearray([0xDD, 0xA5, 0x03, 0x00, 0xFF, 0xFD, 0x77, 0x0D, 0x0A])
                    #print("TX", v)
                    self.central.write(v, with_response)
                    time.sleep_ms(1000)
                except:
                    print("BLE:TX failed")
                    return False
        return True                
                
            
                
    def disconnect(self):
        return self.central.disconnect()
                

    def get_volt(self):
        return self.volt
    
    def get_amps(self):
        return self.amps
    
    def get_power(self):
        return self.power
    
    def get_capAh(self):
        return self.capAh
    
    def get_capPrc(self):
        return self.capPrc
    
    def get_temp1(self):
        return self.temp1
    
    def get_temp2(self):
        return self.temp2
    
    def get_mosfet(self):
        return self.mosfet
    
    def get_temp(self):
        return self.temp
        
    def get_power(self):
        return self.power
    
    def get_energy(self):
        return self.energy
    
    def get_energy_diff(self):
        return self.energy_diff
    
    def get_bms_fully_work(self):
        if ( self.mosfet == 3 ):
            # Charging = enabled
            # Discharge = enabled
            return True
        else:
            return False
         
    def get_scheduled_time(self):
        return self.scheduled_time        


def test2_ble_bms():
    i_bms = Cbms(30)

    while(True):
        if (i_bms.handler()):
            print("")
            print("bms_volt: ",      i_bms.get_volt(), " mV")
            print("bms_amps: ",      i_bms.get_amps(), " mA")
            print("bms_power: ",     i_bms.get_power(), " W")
            
            print("bms_capAh: ",     i_bms.get_capAh(), " mAh")
            print("bms_capPrc: ",    i_bms.get_capPrc(), " %")
            print("bms_temp1",       i_bms.get_temp1())
            print("bms_temp2",       i_bms.get_temp2())
            print("bms_mosfet",      i_bms.get_mosfet())
            
            print("bms_temp",        i_bms.get_temp())
            print("bms_power",       i_bms.get_power(), " A")
            print("bms_energy",      i_bms.get_energy(), " Wh")
            print("bms_energy_diff", i_bms.get_energy_diff(), " Wh")
            print("bms_fully_work", i_bms.get_bms_fully_work())
            
            i_bms.set_energy();
        
        if (i_bms.get_comm_ok()):
            print('+', end='')
        else:
            print('-', end='')
        time.sleep_ms(100)
        

def test3():
    
    bms = Cbms(300) # update information every 300 second
    if(bms.scan_and_connect() == True):
        time.sleep_ms(100)
        print("BMS found");
        bms.disconnect()

if __name__ == "__main__":
    test2_ble_bms()
    #test3()
