from machine import Pin
import time
from machine import reset
import micropython
from machine import I2C, Pin
from esp8266_i2c_lcd import I2cLcd

from ble_bms import Cbms

from machine import WDT


PIN_LED        = 13
PIN_I2C_SCL    = 22
PIN_I2C_SDA    = 21


I2C_ADDR_LCD = 0x27

def free(full=False):
  gc.collect()
  F = gc.mem_free()
  A = gc.mem_alloc()
  T = F+A
  P = '{0:.2f}%'.format(F/T*100)
  if not full: return P
  else : return ('Total:{0} Free:{1} ({2})'.format(T,F,P))



def infoPrints():
    print('time: {:2d}:{:2d}:{:2d}'.format(actual_time[3],actual_time[4],actual_time[5]))    
    print('time: ' , time_stop-time_start , ' ms')
    print(free(True))
    print()
    time.sleep(2)
    
    
# INIT!!!
print('Start 20230207')
led = Pin(PIN_LED, Pin.OUT)
print('LED done')

i2c = I2C(0, scl=Pin(PIN_I2C_SCL), sda=Pin(PIN_I2C_SDA), freq=200000)
print('I2C done')

try:
    lcd = I2cLcd(i2c, I2C_ADDR_LCD, 4, 20)
except:
    print('No LCD1')
    for i in range(2000/50):
        led.value(not led.value())
        time.sleep_ms(50)
    reset()
        
print('LCD done')        

i_bms = Cbms(10) # 10 seconds

while(True):
    i_bms.handler()
    
    if (i_bms.get_comm_ok()):
        lcd.clear()
        lcd.move_to(0,0)
        lcd.putstr('{:6.2f}V'.format(i_bms.volt/1000))
        
        lcd.move_to(0,1)
        lcd.putstr('{:+6.2f}A'.format(i_bms.amps/1000))
        
        lcd.move_to(0,2)
        lcd.putstr('{:+3.1f}W'.format(i_bms.power))
              
        lcd.move_to(0,3)
        lcd.putstr('{:5.2f}Ah  '.format(i_bms.capAh/100))
        lcd.putstr('{:2d}%'.format(i_bms.capPrc))
        
        lcd.move_to(8,0)
        lcd.putstr('{:3.0f}/'.format(i_bms.energy))
        lcd.putstr('{:3.1f}Wh'.format(i_bms.energy_diff))        
        
        lcd.move_to(8,1)
        lcd.putstr('{:3.1f}C'.format(i_bms.temp/10))        
        
        lcd.move_to(8,2)
        lcd.putstr('{:03d}'.format(i_bms.mosfet))
        
        i_bms.set_energy()
        print(".")
        

    lcd.move_to(14,3)
    lcd.putstr('{:4.1f}%'.format(100*(i_bms.ok_counter / i_bms.get_counter)))

    time.sleep_ms(1000)
    
    
        
